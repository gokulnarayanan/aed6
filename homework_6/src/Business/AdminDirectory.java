/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author naray
 */
public class AdminDirectory {
    private ArrayList<Administrator> adminlist;
    public AdminDirectory(){
        adminlist=new ArrayList<Administrator>();
    }
    public ArrayList<Administrator> getAdminlist() {
        return adminlist;
    }

    public void setAdminlist(ArrayList<Administrator> adminlist) {
        this.adminlist = adminlist;
    }
    
    public void addAdmin(Administrator a){
        adminlist.add(a);
    }

}

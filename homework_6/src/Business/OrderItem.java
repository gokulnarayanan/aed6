/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

/**
 *
 * @author Rishika
 */
public class OrderItem {
    
    private int quantity;
    private double salesPrice;
    private double floorPrice;
    private double ceilPrice;
    private Product product;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public double getCeilPrice() {
        return ceilPrice;
    }

    public void setCeilPrice(double ceilPrice) {
        this.ceilPrice = ceilPrice;
    }
    
    @Override
    public String toString(){
       // return product.getProdName();
       return "";
    }
}

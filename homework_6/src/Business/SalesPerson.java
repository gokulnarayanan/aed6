/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author naray
 */
public class SalesPerson {
    String id;
    String username;
    String password;
    int commission;
    boolean salesMade;

    public boolean isSalesMade() {
        return salesMade;
    }

    public void setSalesMade(boolean salesMade) {
        this.salesMade = salesMade;
    }
        public SalesPerson(){
            this.commission=0;
             this.salesMade=false;
        }
    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}

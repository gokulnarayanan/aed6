/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author naray
 */
public class SalesPersonDirectory {
     private List<SalesPerson> salesPersonList;
     public SalesPersonDirectory(){
         salesPersonList=new ArrayList<SalesPerson>();
     }
    public List<SalesPerson> getSalesPersonList() {
        return salesPersonList;
    }


         public SalesPerson addSalesPerson(){
        SalesPerson s = new SalesPerson();
        salesPersonList.add(s);
        return s;
    }
   public void removeSalesPerson(SalesPerson s){
        salesPersonList.remove(s);
    }
   
       public SalesPerson searchSalesPerson(String keyword){
        for (SalesPerson sp : salesPersonList) {
            if(sp.getId().equals(keyword)){
                return sp;
            }
        }
        return null;
    }
}

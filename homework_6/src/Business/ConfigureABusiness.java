/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author naray
 */
public class ConfigureABusiness {
    public static AdminDirectory InitialiseAdministrator(){
        AdminDirectory a=new AdminDirectory();
        Administrator aa=new Administrator();
       aa.setName("Gokul");
       aa.setUsername("u1");
       aa.setPassword("pwd");
       
       Administrator a2=new Administrator();
       a2.setName("Sachin");
       a2.setUsername("u2");
       a2.setPassword("pwd");
       
       a.addAdmin(aa);
       a.addAdmin(a2);
        return a;
    }
        public static SalesPersonDirectory InitialiseSalesPerson() throws IOException{
     SalesPersonDirectory spd=new SalesPersonDirectory();

     try{
             BufferedReader CSV =new BufferedReader(new FileReader("SalesPerson.csv"));
            String data = CSV.readLine();
           while (data != null){
            String[] dataArray = data.split(",");
           SalesPerson s=spd.addSalesPerson();
            s.setId(dataArray[0]);
            s.setUsername(dataArray[1]);
            s.setPassword(dataArray[2]);
          data =CSV.readLine();
           }
     }
        catch(FileNotFoundException ex) {
        System.out.println("Sales File not found"); 
        }
     catch(Exception e){
         System.out.println("Error");
     }
     
     return spd;
    }
        
      public static SupplierDirectory InitializeSupplier() throws IOException{  
          SupplierDirectory sd =new SupplierDirectory();
         
        try{
             BufferedReader CSV =new BufferedReader(new FileReader("SupplierList.csv"));
            String data = CSV.readLine();
           while (data != null){
            String[] dataArray = data.split(",");
            Supplier su=sd.addSupplier();
            su.setSupplyName(dataArray[0]);
            data = CSV.readLine();
           }
          
           
           BufferedReader CSV2 =new BufferedReader(new FileReader("Market.csv"));
            String data2 = CSV2.readLine();
             
            while(data2!=null){
                String[] dataArray = data2.split(",");
                for(Supplier su:sd.getSupplierlist()){
                    ProductCatalog pc=new ProductCatalog();
                    if(su.getSupplyName().equals(dataArray[0])){
                        Product p=new Product();
                                
                        p.setProductName(dataArray[1]);
                        p.setAvailibleNo(Integer.parseInt(dataArray[2]));
                        p.setTargetPrice(Integer.parseInt(dataArray[5]));
                        su.getProductCatalog().getProductCatalog().add(p);
                       
                    }
    

                }
                data2=CSV2.readLine();
            }
            
            CSV.close();
            
        }
        catch(FileNotFoundException ex) {
        System.out.println("Market File not found"); 
        }
        return sd;
      }
}

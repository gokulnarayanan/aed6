/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author avbais
 */
public class Product {
    
    private int productID;
    private String productName;
    private int availibleNo;
    private int floorPrice;
    private int ceilingPrice;
    private int targetPrice;
    private String customerType;
    static int count=0;
    public Product(){
        count++;
        this.productID=count;
    }

    public int getProductID() {
        return productID;
    }

 
  

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getAvailibleNo() {
        return availibleNo;
    }

    public void setAvailibleNo(int availibleNo) {
        this.availibleNo = availibleNo;
    }

    public int getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(int floorPrice) {
        this.floorPrice = floorPrice;
    }

    public int getCeilingPrice() {
        return ceilingPrice;
    }

    public void setCeilingPrice(int ceilingPrice) {
        this.ceilingPrice = ceilingPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    
    public String toString(){
        return this.productName;
    }
}

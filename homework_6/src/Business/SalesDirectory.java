/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author naray
 */
public class SalesDirectory {
    private List<Sales>  salesdirectory;
    
    public SalesDirectory(){
        salesdirectory =new ArrayList<Sales>();
    }

    public List<Sales> getSalesdirectory() {
        return salesdirectory;
    }

    public void setSalesdirectory(List<Sales> salesdirectory) {
        this.salesdirectory = salesdirectory;
    }
    public void addSales(Sales s){
        salesdirectory.add(s);
        
    }
}

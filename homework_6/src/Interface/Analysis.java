/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.MasterOrderCatalog;
import Business.Order;
import Business.OrderItem;
import Business.Product;
import Business.ProductCatalog;
import Business.Sales;
import Business.SalesDirectory;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import Business.Supplier;
import Business.SupplierDirectory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JPanel;

import java.util.Collections;
import java.util.Comparator;
import javax.swing.JOptionPane;

/**
 *
 * @author naray
 */
public class Analysis extends javax.swing.JPanel {
private JPanel userProcessController;
private SalesDirectory sd;
private SalesPersonDirectory spd;
private List<ana> anaList;
private List<ana2> anaList2;
private List<ana3> anaList3;
private ProductCatalog pc;
private MasterOrderCatalog moc;
private SupplierDirectory supd;
    /**
     * Creates new form Analysis
     */
public class CustomComparator implements Comparator<ana3> {
    @Override
    public int compare(ana3 o1, ana3 o2) {
        return o2.revenue-o1.revenue;
    }
}
    public Analysis(JPanel userProcessController, SalesDirectory sd, SalesPersonDirectory spd ,SupplierDirectory supd, MasterOrderCatalog moc ) {
        initComponents();
        this.userProcessController=userProcessController;
        this.sd=sd;
        this.spd=spd;
        this.pc=pc;
        this.moc=moc;
        this.supd=supd;
        populate();
    }
    class ana{
        String salesid;
        int revenue;
        double gap;
        int status;
    }
    class ana2{
        int pid;
       double gap;
       int status=0;
    }
    class ana3{
        String type;
        SalesPerson sp;
        int revenue;
    }
    
    
    public void populate(){

        anaList=new ArrayList<ana>();
        anaList2=new ArrayList<ana2>();
        anaList3=new ArrayList<ana3>();
        double total_sales=0;
        double financial_sales=0;
        double education_sales=0;
        for(SalesPerson sp:spd.getSalesPersonList()){
            if(sp.isSalesMade()){
            ana e=new ana();
            e.salesid=sp.getId();
            e.revenue=0;
            e.gap=0;
            e.status=0;
            anaList.add(e);}
        }
        for(Order o:moc.getOrderCatalog()){
            for(ana e:anaList){
                if(e.salesid==o.getSalesperson().getId()){
                for(OrderItem oi: o.getOrderItemList() ){
                    Product p=oi.getProduct();
                e.revenue=(int) (e.revenue+oi.getSalesPrice());
                 e.gap=e.gap+(oi.getSalesPrice()-p.getTargetPrice());
                 if((oi.getSalesPrice()-p.getTargetPrice())<0){
                     e.status=-1;
                 }
            }}
        }}
        
        for(Supplier s: supd.getSupplierlist()){
            for(Product p: s.getProductCatalog().getProductCatalog()){
            ana2 f=new ana2();
            f.pid=p.getProductID();
            f.gap=0;
            anaList2.add(f);
            }
        }
      for(Order o:moc.getOrderCatalog()){
         for(OrderItem oi:o.getOrderItemList()){
             Product p=oi.getProduct();
             total_sales=total_sales+oi.getSalesPrice();
               if(oi.getType().equals("Educational")){
                   education_sales+=oi.getSalesPrice();
               }
               else if(oi.getType().equals("Financial")){
                   financial_sales+=oi.getSalesPrice();
               }
             for(ana2 f: anaList2){
                 if(p.getProductID()==f.pid){
                     f.gap= f.gap+(oi.getSalesPrice()-p.getTargetPrice());
                     if(oi.getSalesPrice()-p.getTargetPrice()<0){
                         f.status=-1;
                     }
                 }
             }
         }
      }
        
       
        String[] personswWtihBelowTotal=new String [20];
        String[] personswWtihAboveTotal=new String [20];
        int count1=0,count2=0;
        for(ana e:anaList){
            if(e.status<0){
                personswWtihBelowTotal[count1]=e.salesid;
                count1++;
            }
            else{
                personswWtihAboveTotal[count2]=e.salesid;
                count2++;
            }
        }
         System.out.println("Above Total: ");
        for(String s: personswWtihAboveTotal){
            if(s!=null )
            System.out.println(s);
        }
        System.out.println("Below Total: ");
        for(String s: personswWtihBelowTotal){
            if(s!=null)
            System.out.println(s);
        }
        
        
        double max1 = Double.MIN_VALUE;
        double max2 = Double.MIN_VALUE;
        double max3 = Double.MIN_VALUE;  

for (ana2 f: anaList2)
{if(f.status>=0){
    if (f.gap > max1)
    {
        max3 = max2; max2 = max1; max1 = f.gap;
    }
    else if (f.gap > max2)
    {
        max3 = max2; max2 =f.gap;
    }
    else if (f.gap   > max3)
    {
        max3 = f.gap;
    }
}}

    for(ana2 f: anaList2){
       
        if(f.gap==max1)
            l4.setText("Maximum Revenue 1: Product ID:"+f.pid);
        else if(f.gap==max2)
            l5.setText("Maximum Revenue 2: Product ID:"+f.pid);
        else if(f.gap==max3)
            l6.setText("Maximum Revenue 3: Product ID: "+f.pid);
           
    }
        
        for(Order o:moc.getOrderCatalog()){
            ana3 a=new ana3();
            for(OrderItem oi:o.getOrderItemList()){
                a.type=oi.getType();
                a.sp=o.getSalesperson();
                int flag=0;
                for(ana3 q:anaList3){
                    if(q.type==a.type && q.sp==a.sp){
                        q.revenue+=oi.getSalesPrice();
                    flag=1;
                    }
                    
                }
                if(flag==0)
                    a.revenue+=oi.getSalesPrice();
                anaList3.add(a);
            }
        }
        
        
        l1.setText("Total revenue for xerox : "+total_sales);
        l2.setText("Total Revenue in the Financial Market: "+financial_sales);
        l3.setText("Total revenune in the Educational Market: "+education_sales);
        
      
      for(SalesPerson s:spd.getSalesPersonList()){
          if(s.isSalesMade())
          System.out.println("Person "+s.getId()+" Commission "+s.getCommission());
      }
        
      

     Collections.sort(anaList3, new CustomComparator());
        int count3=0;
        System.out.println("Analysis per salesperson by market");
        for(ana3 a:anaList3){
            if(count3<10)
            System.out.println(a.sp.getId()+"  "+a.type+"  "+a.revenue);
            count3++;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        l1 = new javax.swing.JLabel();
        l2 = new javax.swing.JLabel();
        l3 = new javax.swing.JLabel();
        l4 = new javax.swing.JLabel();
        l5 = new javax.swing.JLabel();
        l6 = new javax.swing.JLabel();
        l7 = new javax.swing.JLabel();

        l1.setBorder(new javax.swing.border.MatteBorder(null));

        l2.setBorder(new javax.swing.border.MatteBorder(null));

        l3.setBorder(new javax.swing.border.MatteBorder(null));

        l4.setBorder(new javax.swing.border.MatteBorder(null));

        l5.setBorder(new javax.swing.border.MatteBorder(null));

        l6.setBorder(new javax.swing.border.MatteBorder(null));

        l7.setBorder(new javax.swing.border.MatteBorder(null));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(l1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l2, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l3, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l4, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l5, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l6, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(l7, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(264, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(l1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l5, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(l7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(67, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel l1;
    private javax.swing.JLabel l2;
    private javax.swing.JLabel l3;
    private javax.swing.JLabel l4;
    private javax.swing.JLabel l5;
    private javax.swing.JLabel l6;
    private javax.swing.JLabel l7;
    // End of variables declaration//GEN-END:variables
}
